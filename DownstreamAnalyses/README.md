Downstream analyses in R
================

-   [Background](#background)
-   [Setup the workspace](#setup-the-workspace)
-   [Load the data](#load-the-data)
-   [Library sizes and normalisation](#library-sizes-and-normalisation)
    -   [Library size](#library-size)
    -   [Proportions](#proportions)
    -   [Rarefying](#rarefying)
-   [Alpha diversity](#alpha-diversity)
-   [Beta diversity](#beta-diversity)
    -   [Capscale](#capscale)
-   [Network analysis](#network-analysis)
-   [Differentially abundant taxa](#differentially-abundant-taxa)
-   [Heatmap of differentially abundant taxa](#heatmap-of-differentially-abundant-taxa)
-   [Data wrangling with phyloseq](#data-wrangling-with-phyloseq)
    -   [Accessing your data](#accessing-your-data)
    -   [Merging samples](#merging-samples)
    -   [Agglomerate taxa on different taxonomic levels](#agglomerate-taxa-on-different-taxonomic-levels)
    -   [Prune and subset samples and taxa](#prune-and-subset-samples-and-taxa)

Background
==========

The Atacama soil microbiome data comes from an article in [**mSystems**](https://msystems.asm.org/), [*Significant Impacts of Increasing Aridity on the Arid Soil Microbiome*](https://msystems.asm.org/content/2/3/e00195-16)

The authors studied key environmental and geochemical factors shaping the arid soil microbiome. The key findings are highlighted in the abstract below. For details about the study, you can have a look at the results, especially the figures. We will try to reproduce couple of them in this expercise. The figure 1 below shows the sampling locations.

**Abstract**:

> Global deserts occupy one-third of the Earth’s surface and contribute significantly to organic carbon storage, a process at risk in dryland ecosystems that are highly vulnerable to climate-driven ecosystem degradation. The forces controlling desert ecosystem degradation rates are poorly understood, particularly with respect to the relevance of the arid-soil microbiome. Here we document **correlations between increasing aridity and soil bacterial and archaeal microbiome composition along arid to hyperarid transects** traversing the Atacama Desert, Chile. A meta-analysis reveals that Atacama soil microbiomes exhibit a gradient in composition, are distinct from a broad cross-section of nondesert soils, and yet are similar to three deserts from different continents. **Community richness and diversity were significantly positively correlated with soil relative humidity (SoilRH). Phylogenetic composition was strongly correlated with SoilRH, temperature, and electrical conductivity.** The strongest and most significant correlations between SoilRH and phylum relative abundance were observed for Acidobacteria, Proteobacteria, Planctomycetes, Verrucomicrobia, and Euryarchaeota (Spearman’s rank correlation \[rs\] = &gt;0.81; false-discovery rate \[q\] = ≤0.005), characterized by 10- to 300-fold decreases in the relative abundance of each taxon. In addition, **network analysis revealed a deterioration in the density of significant associations between taxa along the arid to hyperarid gradient**, a pattern that may compromise the resilience of hyperarid communities because they lack properties associated with communities that are more integrated. In summary, results suggest that **arid-soil microbiome stability is sensitive to aridity** as demonstrated by decreased community connectivity associated with the transition from the arid class to the hyperarid class and the **significant correlations observed between soilRH and both diversity and the relative abundances of key microbial phyla** typically dominant in global soils.

![**Figure 1.** Map of site locations for Baquedano (red) and Yungay (yellow) transects (Neilson *et al*, 2017)](https://msystems.asm.org/content/msys/2/3/e00195-16/F1.large.jpg?width=800&height=600&carousel=1)

Setup the workspace
===================

**Before starting you should have downloaded your results to your own laptop using e.g. FileZilla.**

Change the workspace to the folder where you have your data with `setwd()`

``` r
setwd("/PATH/TO/DATA/")
```

Packages are collections of R functions. They are loaded with `library()` function.

``` r
library(phyloseq)
library(vegan)
library(tidyverse)
library(qiime2R)
library(picante)
library(gridExtra)
library(DESeq2)
```

If you're missing some of the packages, you can install most of them either from RStudio's dropdown menu `Tools -> Install Packages...`
Or with `install.packages("PACKAGE_NAME")`.

**tidyverse & vegan & picante & gridExtra:**

``` r
install.packages("tidyverse")
install.packages("vegan")
install.packages("picante")
install.packages("gridExtra")
```

Some of the packages need to be loaded from [Bioconductor](http://www.bioconductor.org/)

**phyloseq & DESeq2:**

``` r
source('http://bioconductor.org/biocLite.R')
biocLite('phyloseq')
BiocManager::install("DESeq2", version = "3.8")
```

And some directly from GitHub

**qiime2R**:

``` r
install.packages("devtools")
library(devtools)
devtools::install_github("jbisanz/qiime2R")
```

After you have installed the missing packages, you still need to load each with `library`.

Load the data
=============

Now everything should be ready and we can start working with the data.
We will use a package called ***phyloseq*** that is meant for working with microbiome data.

First load the data into R and make a `phyloseq` object from it. Phyloseq object can contain an ASV table, a taxonomy table,a sample metadata table, a phylogenetic tree and all of the ASV sequences.

Our phyloseq object will contain the ASV table, the taxonomy table, the phylogenetic tree and the sample metadata table.

``` r
metadata <- read_tsv("sample-metadata.tsv")
ASVs <- read_qza("table.qza")
taxonomy <- read_qza("taxonomy.qza")
tree <- read_qza("rooted-tree.qza")
phy_obj <- qza_to_phyloseq("table.qza", "rooted-tree.qza", "taxonomy.qza","sample-metadata.tsv")
phy_obj
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2847 taxa and 66 samples ]
    ## sample_data() Sample Data:       [ 66 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2847 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2847 tips and 2833 internal nodes ]

We see that our phyloseq object contains ASV (OTU) table with 2847 taxa and 66 samples. Our sample data has 22 sample variables. The taxonomic annotations have 7 levels (Kingdom, Phylumn, Class, Order, Family, Genus, Species) . And finally we have a phylogenetic tree.

If you can't install `qiime2R`.

``` r
# ASV table
ASVs <- read.table("ASV_table.txt", sep="\t", header=TRUE, row.names=1)
# taxonomy
taxonomy <- read.table("taxonomy.txt", sep="\t", header=TRUE, row.names=1)
taxonomy <- as.matrix(taxonomy)
# tree
tree <- read_tree("tree.nwk")
# sample data
metadata <- read.table("metadata.txt", sep="\t", header=TRUE, row.names=1)
# phyloseq object
phy_obj <- phyloseq(otu_table(ASVs, taxa_are_rows = TRUE), 
                    tax_table(taxonomy), sample_data(metadata), tree)
```

The taxonomic annotations look awful after QIIME2, so we better do something about it. Unfortunately we can't do much with the NA's, but at least we can get rid off the `k__`, `p__` etc...

``` r
head(tax_table(phy_obj), 10)
```

    ## Taxonomy Table:     [10 taxa by 7 taxonomic ranks]:
    ##                                  Kingdom       Phylum              
    ## 54882bc3e26adbc6b800a483a451336a "k__Bacteria" NA                  
    ## db2257d2049f8216b47f5020623a3001 "k__Bacteria" NA                  
    ## 98a5a540959fff626ff0753c35b35da7 "k__Bacteria" NA                  
    ## dba1525ac6bd5173c4d5f87ab82acf63 "k__Bacteria" NA                  
    ## c520d6e3cd0edc147f3de2be3d521ac4 "k__Bacteria" NA                  
    ## f22c9286e0b547ee5249483fa7558f05 "k__Bacteria" NA                  
    ## 4165c29513783b471bd2870c568cbce5 "k__Bacteria" NA                  
    ## 024fc04f01784c30df85ccf0bf9c8fe8 "k__Archaea"  "p__[Parvarchaeota]"
    ## c2252c3545754ab07a79ca051971f458 "k__Archaea"  "p__[Parvarchaeota]"
    ## eccdab922afc956b7c34fff6cc92e9cc "k__Archaea"  "p__[Parvarchaeota]"
    ##                                  Class              Order         Family
    ## 54882bc3e26adbc6b800a483a451336a NA                 NA            NA    
    ## db2257d2049f8216b47f5020623a3001 NA                 NA            NA    
    ## 98a5a540959fff626ff0753c35b35da7 NA                 NA            NA    
    ## dba1525ac6bd5173c4d5f87ab82acf63 NA                 NA            NA    
    ## c520d6e3cd0edc147f3de2be3d521ac4 NA                 NA            NA    
    ## f22c9286e0b547ee5249483fa7558f05 NA                 NA            NA    
    ## 4165c29513783b471bd2870c568cbce5 NA                 NA            NA    
    ## 024fc04f01784c30df85ccf0bf9c8fe8 "c__[Parvarchaea]" "o__YLA114"   NA    
    ## c2252c3545754ab07a79ca051971f458 "c__[Parvarchaea]" "o__WCHD3-30" NA    
    ## eccdab922afc956b7c34fff6cc92e9cc "c__[Parvarchaea]" "o__WCHD3-30" NA    
    ##                                  Genus Species
    ## 54882bc3e26adbc6b800a483a451336a NA    NA     
    ## db2257d2049f8216b47f5020623a3001 NA    NA     
    ## 98a5a540959fff626ff0753c35b35da7 NA    NA     
    ## dba1525ac6bd5173c4d5f87ab82acf63 NA    NA     
    ## c520d6e3cd0edc147f3de2be3d521ac4 NA    NA     
    ## f22c9286e0b547ee5249483fa7558f05 NA    NA     
    ## 4165c29513783b471bd2870c568cbce5 NA    NA     
    ## 024fc04f01784c30df85ccf0bf9c8fe8 NA    NA     
    ## c2252c3545754ab07a79ca051971f458 NA    NA     
    ## eccdab922afc956b7c34fff6cc92e9cc NA    NA

``` r
tax_table(phy_obj) <- gsub(".__", "", tax_table(phy_obj))
head(tax_table(phy_obj), 10)
```

    ## Taxonomy Table:     [10 taxa by 7 taxonomic ranks]:
    ##                                  Kingdom    Phylum           
    ## 54882bc3e26adbc6b800a483a451336a "Bacteria" NA               
    ## db2257d2049f8216b47f5020623a3001 "Bacteria" NA               
    ## 98a5a540959fff626ff0753c35b35da7 "Bacteria" NA               
    ## dba1525ac6bd5173c4d5f87ab82acf63 "Bacteria" NA               
    ## c520d6e3cd0edc147f3de2be3d521ac4 "Bacteria" NA               
    ## f22c9286e0b547ee5249483fa7558f05 "Bacteria" NA               
    ## 4165c29513783b471bd2870c568cbce5 "Bacteria" NA               
    ## 024fc04f01784c30df85ccf0bf9c8fe8 "Archaea"  "[Parvarchaeota]"
    ## c2252c3545754ab07a79ca051971f458 "Archaea"  "[Parvarchaeota]"
    ## eccdab922afc956b7c34fff6cc92e9cc "Archaea"  "[Parvarchaeota]"
    ##                                  Class           Order      Family Genus
    ## 54882bc3e26adbc6b800a483a451336a NA              NA         NA     NA   
    ## db2257d2049f8216b47f5020623a3001 NA              NA         NA     NA   
    ## 98a5a540959fff626ff0753c35b35da7 NA              NA         NA     NA   
    ## dba1525ac6bd5173c4d5f87ab82acf63 NA              NA         NA     NA   
    ## c520d6e3cd0edc147f3de2be3d521ac4 NA              NA         NA     NA   
    ## f22c9286e0b547ee5249483fa7558f05 NA              NA         NA     NA   
    ## 4165c29513783b471bd2870c568cbce5 NA              NA         NA     NA   
    ## 024fc04f01784c30df85ccf0bf9c8fe8 "[Parvarchaea]" "YLA114"   NA     NA   
    ## c2252c3545754ab07a79ca051971f458 "[Parvarchaea]" "WCHD3-30" NA     NA   
    ## eccdab922afc956b7c34fff6cc92e9cc "[Parvarchaea]" "WCHD3-30" NA     NA   
    ##                                  Species
    ## 54882bc3e26adbc6b800a483a451336a NA     
    ## db2257d2049f8216b47f5020623a3001 NA     
    ## 98a5a540959fff626ff0753c35b35da7 NA     
    ## dba1525ac6bd5173c4d5f87ab82acf63 NA     
    ## c520d6e3cd0edc147f3de2be3d521ac4 NA     
    ## f22c9286e0b547ee5249483fa7558f05 NA     
    ## 4165c29513783b471bd2870c568cbce5 NA     
    ## 024fc04f01784c30df85ccf0bf9c8fe8 NA     
    ## c2252c3545754ab07a79ca051971f458 NA     
    ## eccdab922afc956b7c34fff6cc92e9cc NA

Library sizes and normalisation
===============================

Library size
------------

To calculate library sizes we can use function `sample_sums`from phyloseq package. Since some of the libraries are pretty small, we need to decide which samples we'll keep.
The line goes at 2000, probably smaller libraries should be removed from this study.

``` r
sums <- sample_sums(phy_obj)
barplot(sums[order(sums, decreasing=TRUE)], las=3, cex.names = 0.5, ylab="Library size", col = "skyblue")
abline(h = 2000, lty=2)
```

![](README_files/figure-markdown_github/norm-1.png)

Proportions
-----------

We can again using a phyloseq function remove the samples with too small library size. You can see that now we have 50 samples left. You should in most cases (if not always) normalise your data one way or another. We will normalise the counts to proportions. Then all the counts add up to one.

``` r
phy_obj <- prune_samples(sample_sums(phy_obj)>2000, phy_obj)
phy_obj
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2847 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2847 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2847 tips and 2833 internal nodes ]

``` r
phy_norm <- transform_sample_counts(phy_obj, function(x) x/sum(x))
barplot(sample_sums(phy_norm), las=3, cex.names = 0.5, col="skyblue")
```

![](README_files/figure-markdown_github/unnamed-chunk-2-1.png)

Rarefying
---------

Another way of normalising is to rarefy the counts. And for that there's also a rarefication function in phyloseq.
We rarefy to the smallest library size after removing the smallest samples without replacement and trim all taxa that have zero count after rarefication.

``` r
set.seed(771)
phy_raref <- rarefy_even_depth(phy_obj, sample.size = min(sample_sums(phy_obj)),
                               replace = FALSE, trimOTUs = TRUE, verbose = TRUE)
phy_raref
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2762 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2762 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2762 tips and 2749 internal nodes ]

``` r
barplot(sample_sums(phy_raref), las=3, cex.names = 0.5, col = "skyblue")
```

![](README_files/figure-markdown_github/rarefy-1.png)

Alpha diversity
===============

Table of alpha diversity measures from the original publication can be downloaded from [here.](https://msystems.asm.org/content/msys/2/3/e00195-16/DC2/embed/inline-supplementary-material-2.pdf?download=true)

In figure 2 the average soil relative humidity is plotted against two different alpha diverity measures. We will try to reproduce the figure.

The Shannon index is quite robust to different sample sizes, so you can use either the normalised, rarefied or raw counts.
For richness estimates we should use the rarefied data, since the seuqencing depth affects the observed richness.

``` r
# Diversity indices
shannon_div <- diversity(otu_table(phy_obj), MARGIN = 2, index = "shannon")
faiths_div <- pd(as.data.frame(t(otu_table(phy_raref))), phy_tree(phy_obj), include.root = FALSE)

# Average humidity
AvgHum <- sample_data(phy_obj)$AverageSoilRelativeHumidity

# Plot the richness against the average humidity
plot(faiths_div$SR~AvgHum, pch=21, cex=2, bg="skyblue", ylab="Faith's PD (Richness)",
     xlab="Average Soil Relative Humidity (%)")
```

![](README_files/figure-markdown_github/alpha-1.png)

``` r
cor.test(faiths_div$SR, AvgHum, method="spearman")
```

    ## 
    ##  Spearman's rank correlation rho
    ## 
    ## data:  faiths_div$SR and AvgHum
    ## S = 4722.6, p-value = 7.204e-09
    ## alternative hypothesis: true rho is not equal to 0
    ## sample estimates:
    ##       rho 
    ## 0.7269563

``` r
# And the Shannon diversity
plot(shannon_div~AvgHum, pch=21, cex=2, bg="skyblue", ylab="Shannon Diversity Index",  
     xlab="Average Soil Relative Humidity (%)")
```

![](README_files/figure-markdown_github/alpha-2.png)

``` r
cor.test(shannon_div, AvgHum, method="spearman")
```

    ## 
    ##  Spearman's rank correlation rho
    ## 
    ## data:  shannon_div and AvgHum
    ## S = 6917.8, p-value = 8.279e-06
    ## alternative hypothesis: true rho is not equal to 0
    ## sample estimates:
    ##       rho 
    ## 0.6000367

We got pretty much the same results as in the original article even with just 10 % of the data.

Beta diversity
==============

In figure 3 beta diversity is combined with soil properties. The beta diversity measure used is unweighted unifrac and the plot is PCoA.
We'll use the `ordinate` to calculate the ordination and `plot_ordination`to plot it. In addtion we'll modiffy the plots using some ggplot2 functions.

``` r
ord <- ordinate(phy_raref, method = "PCoA", distance = "unifrac", weighted=FALSE)

p1 <- plot_ordination(phy_norm, ord, color = "TransectName") + geom_point(size=5) +
  scale_color_manual(values=c("red", "yellow")) + theme_classic() + theme(legend.position = "bottom")

p2 <- plot_ordination(phy_norm, ord, color = "AverageSoilRelativeHumidity") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p3 <- plot_ordination(phy_norm, ord, color = "TemperatureSoilHigh") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p4 <- plot_ordination(phy_norm, ord, color = "EC") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

p5 <- plot_ordination(phy_norm, ord, color = "pH") + geom_point(size=5) +
  scale_color_continuous(low="yellow", high="brown", na.value = "white") + theme_classic() +
  theme(legend.position = "bottom")

layout_mat <- rbind(c(1,1,2),
                    c(3,4,5))

grid.arrange(p1, p2, p3, p4, p5, layout_matrix = layout_mat)
```

![](README_files/figure-markdown_github/beta-1.png)

We can test whether the average soil relative humidity has a significant effect on the community composition with permutational multivariate analysis of variance. The function `adonis` from vegan package can be used for this. We need to remove NA's from the data before running `adonis`.

``` r
tmp <- prune_samples(!is.na(sample_data(phy_obj)$AverageSoilRelativeHumidity), phy_obj)
adonis(t(otu_table(tmp)) ~ AverageSoilRelativeHumidity, data = data.frame(sample_data(phy_obj)))
```

    ## 
    ## Call:
    ## adonis(formula = t(otu_table(tmp)) ~ AverageSoilRelativeHumidity,      data = data.frame(sample_data(phy_obj))) 
    ## 
    ## Permutation: free
    ## Number of permutations: 999
    ## 
    ## Terms added sequentially (first to last)
    ## 
    ##                             Df SumsOfSqs MeanSqs F.Model      R2 Pr(>F)
    ## AverageSoilRelativeHumidity  1    1.2492 1.24920   2.946 0.06144  0.001
    ## Residuals                   45   19.0814 0.42403         0.93856       
    ## Total                       46   20.3306                 1.00000       
    ##                                
    ## AverageSoilRelativeHumidity ***
    ## Residuals                      
    ## Total                          
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Capscale
--------

``` r
tmp <- prune_samples(complete.cases(sample_data(phy_obj)), phy_obj)
plot(capscale(t(otu_table(tmp)) ~ PercentCover + AverageSoilTemperature + AverageSoilRelativeHumidity, 
              distance="bray", data=data.frame(sample_data(tmp))))
```

![](README_files/figure-markdown_github/unnamed-chunk-4-1.png)

Network analysis
================

For the network analysis we make a very simple example using phyloseq functions. To keep things simple, first remove taxa that have total abundance less than 50 counts.

``` r
phy_tmp <- prune_taxa(taxa_sums(phy_raref)>50, phy_raref)
ig <- make_network(phy_tmp, "taxa", distance = "jaccard", max.dist = 0.95)
plot_network(ig, phy_obj, type="taxa", point_size = 5, label=NULL, color="Class", line_alpha = 0.05)
```

![](README_files/figure-markdown_github/network-1.png)

``` r
ig <- make_network(phy_tmp, "samples", distance = "jaccard", max.dist = 0.95)
plot_network(ig, phy_obj, type="samples", point_size = 5, label=NULL, color="Vegetation", line_alpha = 0.05)
```

![](README_files/figure-markdown_github/network-2.png)

Differentially abundant taxa
============================

As a example we'll study how the vegetation affects the abundance of different ASVs in the samples. To keep it simple, we don't consider any other factors that in reality probably have an effect.

We will use package called DESeq2, which uses a negative binomial distribution. DESeq2 is meant for RNA-seq data but works for also for microbiome count data such as ours. You can read more about DESeq2 and the statistics behind from [here](http://www.bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html) and [here.](http://web.stanford.edu/class/bios221/book/Chap-CountData.html)

Convcert the phyloseq object to DESeq2 object and define the factor we want to study (`Vegetation`).

``` r
diagdds = phyloseq_to_deseq2(phy_obj, ~ Vegetation)
diagdds$Vegetation <- relevel(diagdds$Vegetation, ref = "no")
diagdds = DESeq(diagdds, test="Wald", fitType="mean", sfType="poscount")
res = results(diagdds, cooksCutoff = FALSE, alpha = 0.01)

sigtab = cbind(as(res, "data.frame"), as(tax_table(phy_obj)[rownames(res), ], "matrix"))
sigtab[order(sigtab$log2FoldChange),] %>% head(10)
```

    ##                                   baseMean log2FoldChange    lfcSE
    ## 6e4d034e4b967b745639f825dc73ebe6 25.216079     -25.815066 2.744464
    ## 7452a179b9a24c364642eeaade4d266f 11.997785     -24.816436 2.931536
    ## 8800bca0dea2ed79f023f3e709b19076 11.636998     -24.766367 2.929224
    ## 9279403ab31c9b2574b09cef10f08586  5.327803     -23.105961 3.028433
    ## 66d0b2c3b50e8debf40eb0fc0dd559cc  5.677731      -6.389840 2.665760
    ## 45e4be8e16753ffd4bb4872bf6a35f37  4.186833      -5.949854 3.028787
    ## e2b3c5ef478cae4cbabc7577bf139645 24.033215      -5.836321 2.307215
    ## 2e375a45c87660b3ba87a049708443cc  3.682211      -5.765260 2.860141
    ## 9b86760f1d24e78119093f5f08f45f71  3.425279      -5.660941 3.029152
    ## 706d66ebbf49815672e7af1edf3a0ab3  2.932183      -5.436898 3.029490
    ##                                       stat       pvalue         padj
    ## 6e4d034e4b967b745639f825dc73ebe6 -9.406231 5.142472e-21 2.080786e-19
    ## 7452a179b9a24c364642eeaade4d266f -8.465334 2.554193e-17 3.808069e-16
    ## 8800bca0dea2ed79f023f3e709b19076 -8.454924 2.792698e-17 3.816687e-16
    ## 9279403ab31c9b2574b09cef10f08586 -7.629676 2.353440e-14 2.412276e-13
    ## 66d0b2c3b50e8debf40eb0fc0dd559cc -2.397005 1.652971e-02 6.376659e-02
    ## 45e4be8e16753ffd4bb4872bf6a35f37 -1.964435 4.947970e-02 1.267917e-01
    ## e2b3c5ef478cae4cbabc7577bf139645 -2.529596 1.141940e-02 4.681955e-02
    ## 2e375a45c87660b3ba87a049708443cc -2.015726 4.382868e-02 1.218289e-01
    ## 9b86760f1d24e78119093f5f08f45f71 -1.868820 6.164780e-02 1.438097e-01
    ## 706d66ebbf49815672e7af1edf3a0ab3 -1.794658 7.270821e-02 1.548590e-01
    ##                                   Kingdom         Phylum
    ## 6e4d034e4b967b745639f825dc73ebe6 Bacteria Actinobacteria
    ## 7452a179b9a24c364642eeaade4d266f Bacteria     Firmicutes
    ## 8800bca0dea2ed79f023f3e709b19076 Bacteria Proteobacteria
    ## 9279403ab31c9b2574b09cef10f08586 Bacteria     Firmicutes
    ## 66d0b2c3b50e8debf40eb0fc0dd559cc Bacteria Proteobacteria
    ## 45e4be8e16753ffd4bb4872bf6a35f37 Bacteria     Firmicutes
    ## e2b3c5ef478cae4cbabc7577bf139645 Bacteria Actinobacteria
    ## 2e375a45c87660b3ba87a049708443cc Bacteria Actinobacteria
    ## 9b86760f1d24e78119093f5f08f45f71 Bacteria Actinobacteria
    ## 706d66ebbf49815672e7af1edf3a0ab3 Bacteria Actinobacteria
    ##                                                Class               Order
    ## 6e4d034e4b967b745639f825dc73ebe6           MB-A2-108           0319-7L14
    ## 7452a179b9a24c364642eeaade4d266f             Bacilli          Bacillales
    ## 8800bca0dea2ed79f023f3e709b19076 Gammaproteobacteria       Legionellales
    ## 9279403ab31c9b2574b09cef10f08586             Bacilli          Bacillales
    ## 66d0b2c3b50e8debf40eb0fc0dd559cc Gammaproteobacteria                <NA>
    ## 45e4be8e16753ffd4bb4872bf6a35f37             Bacilli          Bacillales
    ## e2b3c5ef478cae4cbabc7577bf139645      Actinobacteria     Actinomycetales
    ## 2e375a45c87660b3ba87a049708443cc     Thermoleophilia Solirubrobacterales
    ## 9b86760f1d24e78119093f5f08f45f71      Actinobacteria     Actinomycetales
    ## 706d66ebbf49815672e7af1edf3a0ab3     Thermoleophilia Solirubrobacterales
    ##                                             Family        Genus    Species
    ## 6e4d034e4b967b745639f825dc73ebe6              <NA>         <NA>       <NA>
    ## 7452a179b9a24c364642eeaade4d266f  Paenibacillaceae Ammoniphilus       <NA>
    ## 8800bca0dea2ed79f023f3e709b19076              <NA>         <NA>       <NA>
    ## 9279403ab31c9b2574b09cef10f08586  Paenibacillaceae Ammoniphilus       <NA>
    ## 66d0b2c3b50e8debf40eb0fc0dd559cc              <NA>         <NA>       <NA>
    ## 45e4be8e16753ffd4bb4872bf6a35f37       Bacillaceae     Bacillus       <NA>
    ## e2b3c5ef478cae4cbabc7577bf139645   Nocardioidaceae         <NA>       <NA>
    ## 2e375a45c87660b3ba87a049708443cc              <NA>         <NA>       <NA>
    ## 9b86760f1d24e78119093f5f08f45f71 Streptomycetaceae Streptomyces mashuensis
    ## 706d66ebbf49815672e7af1edf3a0ab3              <NA>         <NA>       <NA>

``` r
plot(t(otu_table(phy_obj)["45e4be8e16753ffd4bb4872bf6a35f37",]) ~ sample_data(phy_obj)$Vegetation,
     ylab="raw ASV count", xlab="Vegetation", main = "ASV: 45e4be8e16753ffd4bb4872bf6a35f37\nBacillus")
```

![](README_files/figure-markdown_github/diff_abund-1.png)

Heatmap of differentially abundant taxa
=======================================

``` r
library(pheatmap)
diff_taxa <- row.names(sigtab[order(sigtab$log2FoldChange),]) %>% head(20)
phy_diff <-  prune_taxa(diff_taxa, phy_norm)
phy_diff <- prune_samples(sample_sums(phy_diff)>0, phy_diff)
pheatmap(sqrt(t(otu_table(phy_diff))), labels_col = tax_table(phy_diff)[,6],
         labels_row = sample_data(phy_diff)$Vegetation, cluster_cols = FALSE, angle_col = 45)
```

![](README_files/figure-markdown_github/heatmap-1.png)

Data wrangling with phyloseq
============================

Some useful functions in phyloseq for wrangling your data before analyses. Some you have already used and here's a few more. Before you use any of these to you're own data, read the manual and be sure what it actually does. More examples [here](https://joey711.github.io/phyloseq/) and full documentation with `?function_name`

Accessing your data
-------------------

In addition to the `otu_table`, `tax_table` and other functions to access your data, there's also some other very useful functions.

``` r
phy_obj
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2847 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2847 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2847 tips and 2833 internal nodes ]

``` r
ntaxa(phy_obj)
```

    ## [1] 2847

``` r
nsamples(phy_obj)
```

    ## [1] 50

``` r
sample_names(phy_obj) %>% head
```

    ## [1] "BAQ2420.1.1" "BAQ2420.1.2" "BAQ2420.1.3" "BAQ2420.2"   "BAQ2420.3"  
    ## [6] "BAQ2462.1"

``` r
rank_names(phy_obj)
```

    ## [1] "Kingdom" "Phylum"  "Class"   "Order"   "Family"  "Genus"   "Species"

``` r
sample_variables(phy_obj)
```

    ##  [1] "BarcodeSequence"                 "LinkerPrimerSequence"           
    ##  [3] "Elevation"                       "ExtractConcen"                  
    ##  [5] "AmpliconConcentration"           "ExtractGroupNo"                 
    ##  [7] "TransectName"                    "SiteName"                       
    ##  [9] "Depth"                           "pH"                             
    ## [11] "TOC"                             "EC"                             
    ## [13] "AverageSoilRelativeHumidity"     "RelativeHumiditySoilHigh"       
    ## [15] "RelativeHumiditySoilLow"         "PercentRelativeHumiditySoil_100"
    ## [17] "AverageSoilTemperature"          "TemperatureSoilHigh"            
    ## [19] "TemperatureSoilLow"              "Vegetation"                     
    ## [21] "PercentCover"                    "Description"

``` r
taxa_names(phy_obj) %>% head
```

    ## [1] "54882bc3e26adbc6b800a483a451336a" "db2257d2049f8216b47f5020623a3001"
    ## [3] "98a5a540959fff626ff0753c35b35da7" "dba1525ac6bd5173c4d5f87ab82acf63"
    ## [5] "c520d6e3cd0edc147f3de2be3d521ac4" "f22c9286e0b547ee5249483fa7558f05"

Merging samples
---------------

If you look at the map, there's fewer locations than we have samples. There is several samples from each location. Although they are nto actually replicates, we treat them as relicates now and will combine these together using the variable `SiteName`in the metadata.

**Beware** that the function sums up the counts, even if you specify `fun = "mean"`, so you need to divide the counts with the number of replicates you have.

``` r
phy_mrg <- merge_samples(phy_obj, group = "SiteName", fun = "mean")
barplot(sample_sums(phy_mrg), las=3, main="Sum of counts", col = "skyblue")
```

![](README_files/figure-markdown_github/merge-1.png)

``` r
otu_table(phy_mrg) <- otu_table(phy_mrg)[, ]/as.matrix(table(sample_data(phy_obj)$SiteName))[, 1]
barplot(sample_sums(phy_mrg), las=3, main="Mean counts", col = "skyblue")
```

![](README_files/figure-markdown_github/merge-2.png)

Agglomerate taxa on different taxonomic levels
----------------------------------------------

This function lets you count taxa abundances on different taxonomic levels. **Beware**, this function is very slow.

``` r
phy_glom <- tax_glom(phy_obj, taxrank = "Phylum")
tax_table(phy_glom)
```

    ## Taxonomy Table:     [29 taxa by 7 taxonomic ranks]:
    ##                                  Kingdom    Phylum             Class Order
    ## c2252c3545754ab07a79ca051971f458 "Archaea"  "[Parvarchaeota]"  NA    NA   
    ## cf400229268468ae928415e072520baf "Archaea"  "Euryarchaeota"    NA    NA   
    ## a56b903521b8ab33a7878b35582803a8 "Archaea"  "Crenarchaeota"    NA    NA   
    ## 6aae0e15833d1c9e5eb9e1bdbcbfc5f9 "Bacteria" "TM7"              NA    NA   
    ## d8b88cb4d655b49212c6fcdf9af0881c "Bacteria" "OD1"              NA    NA   
    ## 9373ea979e13388215e7529b3a5af31a "Bacteria" "Cyanobacteria"    NA    NA   
    ## 4e047b3e37ce39a9dc56385b6300cfa9 "Bacteria" "WPS-2"            NA    NA   
    ## ef3fdbe1dcde754d91130cde6a4b4d61 "Bacteria" "Gemmatimonadetes" NA    NA   
    ## 934c8111269ab3c4a97f975ea1da1dab "Bacteria" "[Thermi]"         NA    NA   
    ## 51b4f8f9c1907f09bc2907195333e2e7 "Bacteria" "FBP"              NA    NA   
    ## 1399c57b521cef2b79aff2473842641d "Bacteria" "Elusimicrobia"    NA    NA   
    ## 229f084b1e96e506bf8ff248e7b7b699 "Bacteria" "BRC1"             NA    NA   
    ## e7e19b672a061e119437a7cf815db964 "Bacteria" "Acidobacteria"    NA    NA   
    ## ddbc32632c9632d1dd746f28721f3a9f "Bacteria" "Firmicutes"       NA    NA   
    ## cdb9c0ee3bba4c3d8b9292eb575bd9e3 "Bacteria" "Chloroflexi"      NA    NA   
    ## bb273f4a78a0886761dd79370d9a552d "Bacteria" "GAL15"            NA    NA   
    ## 95c97ef6642c81a0a57e8df5c848c8ef "Bacteria" "Fibrobacteres"    NA    NA   
    ## 3e2f2c4810749252000d9ef3bfbefba3 "Bacteria" "Chlorobi"         NA    NA   
    ## c3bd77a0f6486c74b4b713ce8a5f285b "Bacteria" "Bacteroidetes"    NA    NA   
    ## 5333c18319a32edffa11d0417a3dd6d9 "Bacteria" "Planctomycetes"   NA    NA   
    ## 409faa5f5353e543bf6d99125c7c0e83 "Bacteria" "Proteobacteria"   NA    NA   
    ## ebae8b5d104a84994cefd775af965d48 "Bacteria" "AD3"              NA    NA   
    ## 623a7a25f661a61466559f4205c94e95 "Bacteria" "OP3"              NA    NA   
    ## 2dcce6012bdc7a9cb15900ee79beb2ed "Bacteria" "TM6"              NA    NA   
    ## 0013c1743927ee19a962b903b8990896 "Bacteria" "Verrucomicrobia"  NA    NA   
    ## c4aa988abfb184180567af011d95da47 "Bacteria" "Armatimonadetes"  NA    NA   
    ## 7158b5b009219b1fc3e89f0faeedd765 "Bacteria" "Nitrospirae"      NA    NA   
    ## 915745cfbce346c29583b138a3c5f192 "Bacteria" "WS3"              NA    NA   
    ## a7b877ae6d2f079a15b6b192a4425620 "Bacteria" "Actinobacteria"   NA    NA   
    ##                                  Family Genus Species
    ## c2252c3545754ab07a79ca051971f458 NA     NA    NA     
    ## cf400229268468ae928415e072520baf NA     NA    NA     
    ## a56b903521b8ab33a7878b35582803a8 NA     NA    NA     
    ## 6aae0e15833d1c9e5eb9e1bdbcbfc5f9 NA     NA    NA     
    ## d8b88cb4d655b49212c6fcdf9af0881c NA     NA    NA     
    ## 9373ea979e13388215e7529b3a5af31a NA     NA    NA     
    ## 4e047b3e37ce39a9dc56385b6300cfa9 NA     NA    NA     
    ## ef3fdbe1dcde754d91130cde6a4b4d61 NA     NA    NA     
    ## 934c8111269ab3c4a97f975ea1da1dab NA     NA    NA     
    ## 51b4f8f9c1907f09bc2907195333e2e7 NA     NA    NA     
    ## 1399c57b521cef2b79aff2473842641d NA     NA    NA     
    ## 229f084b1e96e506bf8ff248e7b7b699 NA     NA    NA     
    ## e7e19b672a061e119437a7cf815db964 NA     NA    NA     
    ## ddbc32632c9632d1dd746f28721f3a9f NA     NA    NA     
    ## cdb9c0ee3bba4c3d8b9292eb575bd9e3 NA     NA    NA     
    ## bb273f4a78a0886761dd79370d9a552d NA     NA    NA     
    ## 95c97ef6642c81a0a57e8df5c848c8ef NA     NA    NA     
    ## 3e2f2c4810749252000d9ef3bfbefba3 NA     NA    NA     
    ## c3bd77a0f6486c74b4b713ce8a5f285b NA     NA    NA     
    ## 5333c18319a32edffa11d0417a3dd6d9 NA     NA    NA     
    ## 409faa5f5353e543bf6d99125c7c0e83 NA     NA    NA     
    ## ebae8b5d104a84994cefd775af965d48 NA     NA    NA     
    ## 623a7a25f661a61466559f4205c94e95 NA     NA    NA     
    ## 2dcce6012bdc7a9cb15900ee79beb2ed NA     NA    NA     
    ## 0013c1743927ee19a962b903b8990896 NA     NA    NA     
    ## c4aa988abfb184180567af011d95da47 NA     NA    NA     
    ## 7158b5b009219b1fc3e89f0faeedd765 NA     NA    NA     
    ## 915745cfbce346c29583b138a3c5f192 NA     NA    NA     
    ## a7b877ae6d2f079a15b6b192a4425620 NA     NA    NA

Prune and subset samples and taxa
---------------------------------

Different ways of getting rid off of samples or taxa from your data.

``` r
prune_samples(sample_sums(phy_obj)>2000, phy_obj)
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2847 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2847 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2847 tips and 2833 internal nodes ]

``` r
prune_taxa(taxa_sums(phy_obj)>100, phy_obj)
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 460 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 460 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 460 tips and 458 internal nodes ]

``` r
filter_taxa(phy_obj, function(x) sum(x>0) > (0.5*length(x)), TRUE)
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 4 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 4 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 4 tips and 3 internal nodes ]

``` r
subset_samples(phy_obj, TransectName=="Baquedano")
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2847 taxa and 24 samples ]
    ## sample_data() Sample Data:       [ 24 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2847 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2847 tips and 2833 internal nodes ]

``` r
subset_taxa(phy_obj, Kingdom=="Bacteria")
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 2792 taxa and 50 samples ]
    ## sample_data() Sample Data:       [ 50 samples by 22 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 2792 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 2792 tips and 2778 internal nodes ]
